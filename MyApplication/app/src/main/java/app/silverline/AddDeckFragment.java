package app.silverline;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddDeckFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddDeckFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddDeckFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    private DatabaseReference db;
    private EditText deckNameEdit;
    private Button addDeckButton;
    public AddDeckFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment AddDeckFragment.
     */
    public static AddDeckFragment newInstance() {
        AddDeckFragment fragment = new AddDeckFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (AddDeckFragment.OnFragmentInteractionListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_deck, container, false);
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        db = mListener.getDbRference();
    }

    @Override
    public void onStart() {
        super.onStart();
        View view = getView();
        view.bringToFront();
        deckNameEdit = view.findViewById(R.id.deck_name_edit);
        addDeckButton = view.findViewById(R.id.add_deck_button);

        addDeckButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                addDeck();
                mListener.closeFragment();
            }
        });
    }

    /**
     * create and commit a deck to database
     */
    public void addDeck() {
        String name = deckNameEdit.getText().toString().trim();

        if(!TextUtils.isEmpty(name)) {
            String id = db.push().getKey();

            Deck deck = new Deck(id, name);
            db.child(id).setValue(deck, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError,
                                       @NonNull DatabaseReference databaseReference) {
                    if (null != databaseError)
                        Log.d("database", databaseError.getMessage());
                }
            });

            Toast.makeText(getView().getContext(), "Deck (name) added", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getView().getContext(), "You should enter a name", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
        DatabaseReference getDbRference();
        void closeFragment();
    }
}
