package app.silverline;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;


public class SelectGameMode extends AppCompatActivity {
    private static final int DECKS_DATA_REQUEST = 0;
    private Button playgame;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gamemode);
        playgame = findViewById(R.id.Confirmbutton);
        final Context context = this;
        Intent loadDecks = new Intent(context, ManageDecksActivity.class);
        loadDecks.putExtra("returnToGame", true);
        startActivityForResult(loadDecks, DECKS_DATA_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(resultCode, resultCode, data);
        Log.d("Intent", String.format("Request code: %s", requestCode));
        if (requestCode == DECKS_DATA_REQUEST){
            Log.d("Intent", String.format("Result code: %s", resultCode));
            if (resultCode == Activity.RESULT_OK){
                Log.d("Activity", "Got decks data back.");
                final ArrayList<Deck> decks =
                        (ArrayList<Deck>)data.getSerializableExtra("decks");
                System.out.println(decks);
                // add deck names to options
                final Spinner spinner = findViewById(R.id.spinner);
                ArrayAdapter<String> spinnerAdapter =
                        new ArrayAdapter<String>(this,
                                android.R.layout.simple_spinner_item,
                                android.R.id.text1);
                spinnerAdapter.setDropDownViewResource(
                        android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(spinnerAdapter);
                for (Deck deck: decks){
                    spinnerAdapter.add(deck.getName());
                }
                spinnerAdapter.notifyDataSetChanged();
                // limits the number of Questions to maximum of decks.size()
                final EditText limitQs = findViewById(R.id.numberOfQuestions);
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView,
                                               View selectedItemView,
                                               final int position, long id) {
                        Log.d("Spinner", String.format("Deck %s selected",
                                decks.get(position).getName()));
                        limitQs.setFilters(new InputFilter[]{
                                new MaxNumberOfQuestions(decks.get(position).getListOfProfiles().size())
                        });
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {

                    }
                });
                // set button onclick
                playgame.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view){
                        Deck deck = decks.get(spinner.getSelectedItemPosition());
                        if (deck.getListOfProfiles().size() == 0) {
                            Toast.makeText(SelectGameMode.this,
                                    "The selected Deck has no profile.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (limitQs.getText().length() == 0) {
                            Toast.makeText(SelectGameMode.this,
                                    "How many questions do you want?.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Intent gameIntent = new Intent(SelectGameMode.this,
                                GameActivity.class);

                        gameIntent.putExtra("deck", deck);
                        gameIntent.putExtra("limit",
                                Integer.parseInt(limitQs.getText().toString()));
                        startActivity(gameIntent);
                    }
                });
            }
        }
    }

    public class MaxNumberOfQuestions implements InputFilter {
        int limit;
        public MaxNumberOfQuestions(int size) {
            this.limit = size;
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            try {
                int input = Integer.parseInt(dest.subSequence(0, dstart).toString() + source + dest.subSequence(dend, dest.length()));
                if (input <= limit && input > 0)
                    return null;
            } catch (NumberFormatException nfe) { }
            return "";
        }
    }
}
