package app.silverline;

import java.io.Serializable;

public class Profile implements Serializable {
    private String name;
    private String imagePath;

    public Profile() {}
    public Profile(String name, String imagePath) {
        this.name = name;
        this.imagePath = imagePath;
    }

    /**
     * Sets the name of the person in the profile
     * @param name The string representing the name of the person
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the name of the person in the profile
     * @return The string representing the name of the person in the profile
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the url of the image
     * @return The url string of the image
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     * Sets the url of the image
     * @param imagePath The url string of the image
     */
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

}
