package app.silverline;

import android.support.annotation.NonNull;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a deck
 */
public class Deck implements Comparable, Serializable {
    private String identifier;
    private String name;
    private ArrayList<Profile> listOfProfiles;

    /**
     * Empty constructor
     */
    public Deck(){}

    /**
     * Create Deck with identifier and name
     * @param identifier The unique identifier of the deck
     * @param name Name of deck
     */
    public Deck(String identifier, String name) {
        this.identifier = identifier;
        this.name = name;
        this.listOfProfiles = new ArrayList<>();
    }

    /**
     * Create Deck with identifier and name and list of profiles
     * @param identifier The unique identifier of the deck
     * @param name Name of deck
     * @param profiles List of profiles
     */
    public Deck(String identifier, String name, ArrayList<Profile> profiles) {
        this.identifier = identifier;
        this.name = name;
        this.listOfProfiles = profiles;
    }

    /**
     * Gets list of profiles in the deck
     * @return A list of profiles in the deck
     */
    public ArrayList<Profile> getListOfProfiles() {
        return listOfProfiles;
    }

    /**
     * Sets list of profiles to the deck
     * @param listOfProfiles List of profiles to add to the deck
     */
    public void setListOfProfiles(ArrayList<Profile> listOfProfiles) {
        this.listOfProfiles = listOfProfiles;
    }

    /**
     * Gets the identifier of the deck
     * @return The identifier of the deck
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Sets identifier of the deck
     * @param identifier Unique identifier of the deck
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * Gets the name of the deck
     * @return The name of the deck
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of deck
     * @param name The name of deck
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the fields of the deck in key value format
     * @return A map of all the fields in the deck
     */
    public Map toMap(){
        Map myMap = new HashMap();
        myMap.put("Identifier", identifier);
        myMap.put("Name", name);
        myMap.put("Profiles", listOfProfiles);
        return myMap;
    }

    /**
     * @return The string representation of the Deck
     */
    public String toString(){
        return toMap().toString();
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return identifier.compareTo(((Deck)o).getIdentifier());
    }
}
