package app.silverline;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ManageDecksActivity extends AppCompatActivity
        implements AddDeckFragment.OnFragmentInteractionListener{
    FloatingActionButton addButton;
    DatabaseReference deckDatabase;
    ArrayList<Deck> decks;
    ListView deckList;

    /**
     * Toggle the visibility of the Add fragment
     * @param visibility An integer representing the target state (1: VISIBLE, 8: GONE)
     * @param addDeckContainer The view representing the parent of AddFragment
     */
    public void setAddFragmentVisibility(int visibility, RelativeLayout addDeckContainer) {
        if (addDeckContainer == null) addDeckContainer = findViewById(R.id.add_deck_container);
        addDeckContainer.setVisibility(visibility);

    }

    /**
     * Closes AddDeck fragment after it is used
     */
    public void closeAddDeckFragment(){
        setAddFragmentVisibility(View.GONE, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Context self = this;
        decks = new ArrayList<Deck>();
        deckDatabase = FirebaseDatabase.getInstance().getReference();
        deckDatabase.keepSynced(true);
        deckDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Deck deck = null;
                String identifer = dataSnapshot.child("identifier").getValue(String.class);
                String name = dataSnapshot.child("name").getValue(String.class);
                ArrayList<Profile> profiles = new ArrayList<Profile>();
                for (DataSnapshot child : dataSnapshot.child("listOfProfiles").getChildren()){ // this is the list of Profiles
                    // child is a hashmap of (key: Profiles object)
                    String profileName = child.child("name").getValue(String.class);
                    String imagePath = child.child("imagePath").getValue(String.class);
                    Profile temp = new ProfileBuilder().setName(profileName).setImagePath(imagePath).createProfile();
                    profiles.add(temp);
                }
                if (identifer != null && name != null)
                    deck = new Deck(identifer, name, profiles);
                if (deck != null && !decks.contains(deck)) {
                    decks.add(deck);
                    Log.d("FIREBASE", String.format("Deck %s loaded.", deck.getIdentifier()));
                } else {
                    Toast.makeText(self, "You have no deck.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        if (!getIntent().getBooleanExtra("returnToGame", false))
            setContentView(R.layout.activity_manage);
        deckDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (getIntent().getBooleanExtra("returnToGame", false)){
                    returnToGame();
                } else {
                    DecksAdapter decksAdapter = new DecksAdapter(self, decks);
                    deckList.setAdapter(decksAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        if (!getIntent().getBooleanExtra("returnToGame", false)) {
            deckList = findViewById(R.id.deckList);
            addButton = findViewById(R.id.addDeck);
            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    RelativeLayout addDeckContainer = findViewById(R.id.add_deck_container);
                    setAddFragmentVisibility(View.GONE - addDeckContainer.getVisibility(), addDeckContainer);
                }
            });
            findViewById(R.id.add_deck_container).setOnTouchListener(
                    new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            v.onTouchEvent(event);
                            return true;
                        }
                    }
            );
        }
    }

    /**
     * Returns to GameActivity after loading all the data
     */
    void returnToGame(){
        Log.d("Activity", "Request from SelectGameMode");
        Intent decksIntent = new Intent(this, SelectGameMode.class);
        decksIntent.putExtra("decks", decks);
        setResult(Activity.RESULT_OK, decksIntent);
        finish();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public DatabaseReference getDbRference() {
        return deckDatabase;
    }

    @Override
    public void closeFragment() {
        closeAddDeckFragment();
    }

    class DecksAdapter extends ArrayAdapter<Deck> {
        private Context context;
        public DecksAdapter(Context context, ArrayList<Deck> decks) {
            super(context, 0, decks);
            this.context = context;
        }

        @Override
        public int getCount() {
            return decks.size();
        }

        @Override
        public Deck getItem(int i) {
            return decks.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            final Deck deck = getItem(i);
            if (view == null){
                view = getLayoutInflater().inflate(R.layout.deck_button, viewGroup, false);
            }
            Button deckButton = view.findViewById(R.id.deck);
            deckButton.setText(deck.getName());
            deckButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    System.out.println(deck);
                    Intent deckIntent = new Intent(context, DeckActivity.class);
                    deckIntent.putExtra("deck", deck);
                    startActivity(deckIntent);
                }
            });
            return view;
        }
    }
}
