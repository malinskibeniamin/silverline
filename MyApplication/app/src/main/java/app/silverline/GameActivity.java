package app.silverline;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class GameActivity extends AppCompatActivity {
    private final String[] RANDOM_NAMES = {
        "Barack Obama", "Lee Kwan Yew", "Donald Trump", "Kim Jong Un",
        "Kyle Nguyen", "Benjamin Malinski", "Zixiang", "Nicholas Phang", "Soggeh", "Chris"
    };
    StorageReference storageReference;
    Picasso picasso;
    private Button buttonAnswerFirst;
    private Button buttonAnswerSecond;
    private Button buttonAnswerThird;
    private Button buttonAnswerFourth;
    private int limit;
    private ImageView person;

    private ArrayList<Profile> profiles;

    private Random randomGenerator;

    private int turn;

    private int score;

    /**
     * @return The integer representing the score of the game
     */
    public int getScore() {
        return score;
    }

    /**
     * Create an alert box when the game ends
     */
    public void createAlertUponGameFinish() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(GameActivity.this);

        alertDialogBuilder
                .setMessage("Game over! Score: " + score + "/" + limit)
                .setCancelable(false)
                .setNegativeButton("EXIT",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                .setPositiveButton("RETRY",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                recreate();
                            }
                        })
                .setNeutralButton("STATISTICS",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(getApplicationContext(), StatisticsActivity.class);
                                intent.putExtra("correctAnswers", getScore());
                                intent.putExtra("answers", limit);
                                startActivity(intent);
                                finish();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * Gets random indices from options list
     * @param except The index of the correct answer
     * @return The list of indices for getting options
     */
    public List<Integer> getRandomisedDistinctIndices(int except) {
        List<Integer> numbers = new ArrayList<>();
        while (numbers.size() < profiles.size()-1) {
            int random = randomGenerator.nextInt(profiles.size());
            if (random != except && !numbers.contains(random)) {
                numbers.add(random);
            }
        }

        return numbers;
    }

    /**
     * Assigns the correct answer to a button
     * @param correctAnswer The string representing the correct answer of the current question
     */
    public void assignAnswersToButtons(String correctAnswer) {
        List<Integer> randomisedDistinctIndices = getRandomisedDistinctIndices(turn-1);
        List<String> names = new ArrayList<String>();
        names.add(correctAnswer);
        Iterator<Integer> it = randomisedDistinctIndices.iterator();
        HashSet<Integer> usedName = new HashSet<Integer>();
        while (names.size() < 4){
            if (it.hasNext()) names.add(profiles.get(it.next()).getName());
            else { // get extra names from RANDOM_NAMES
                int index;
                do {
                    index = randomGenerator.nextInt(RANDOM_NAMES.length);
                } while (usedName.contains(index) || names.contains(RANDOM_NAMES[index]));
                names.add(RANDOM_NAMES[index]);
            }
        }
        Collections.shuffle(names);
        setTextForButtons(names);
    }

    /**
     * Sets the string of options to buttons
     * @param names List of string representing the options
     */
    public void setTextForButtons(List<String> names) {
        buttonAnswerFirst.setText(names.get(0));
        buttonAnswerSecond.setText(names.get(1));
        buttonAnswerThird.setText(names.get(2));
        buttonAnswerFourth.setText(names.get(3));
    }

    /**
     * Assigns the image of a profile to the question
     * @param number: an integer representing the index of the profile in question
     * @return The string representing the name of the subject in the image
     */
    public String assignImageToQuestion(int number) {
        Profile profile = profiles.get(number - 1);
        StorageReference ref = storageReference.child(profile.getImagePath());
        ref.getDownloadUrl()
            .addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    picasso.with(GameActivity.this).load(uri.toString()).into(person);
                }
            });
        return profile.getName();
    }

    /**
     * Generates the question with index number
     * @param number The integer representing the index of the profile
     */
    public void generateQuestion(int number) {
        String correctAnswer = assignImageToQuestion(number);
        assignAnswersToButtons(correctAnswer);
    }

    /**
     * Continues the game flow
     * @param list List of profiles
     */
    public void findMoreQuestions(ArrayList<Profile> list) {
        if (turn < limit) {
            turn += 1;
            generateQuestion(turn);
        } else {
            createAlertUponGameFinish();
        }
    }

    /**
     * Validate the answer
     * @param buttonAnswer The button pressed by user
     * @param list List of profiles
     */
    public void validateAnswerCorrectness(Button buttonAnswer, ArrayList<Profile> list) {
        if (buttonAnswer.getText().equals(list.get(turn - 1).getName())) {
            score += 1;
        }

        findMoreQuestions(list);
    }

    /**
     * This function will be called every time any of the answer buttons is clicked.
     * It removes the need to use onClickListeners for each button separately.
     */
    public void onButtonClick(View view) {
        validateAnswerCorrectness(((Button) view), profiles);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Deck deck = (Deck) getIntent().getSerializableExtra("deck");
        limit = getIntent().getIntExtra("limit", 1);
        profiles = deck.getListOfProfiles();
        storageReference = FirebaseStorage.getInstance().getReference();
        person = findViewById(R.id.person);
        buttonAnswerFirst = findViewById(R.id.buttonAnswerFirst);
        buttonAnswerSecond = findViewById(R.id.buttonAnswerSecond);
        buttonAnswerThird = findViewById(R.id.buttonAnswerThird);
        buttonAnswerFourth = findViewById(R.id.buttonAnswerFourth);
        picasso = new Picasso.Builder(GameActivity.this)
                .build();
        picasso.setIndicatorsEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        randomGenerator = new Random();
        turn = 1;
        score = 0;
        Collections.shuffle(profiles);
        generateQuestion(turn);
    }
}
