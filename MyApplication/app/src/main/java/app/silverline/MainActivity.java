package app.silverline;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button play;
    private Button manage;
    private Button statistics;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        play = findViewById(R.id.playgame);
        manage = findViewById(R.id.manage);
        statistics = findViewById(R.id.statistics);

        final Context context = this;
        play.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view){
                Intent selectGameIntent = new Intent(context, SelectGameMode.class);
                startActivity(selectGameIntent);
            }
        });

        manage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent manageDecksIntent = new Intent(context, ManageDecksActivity.class);
                startActivity(manageDecksIntent);
            }
        });

        statistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent statisticsIntent = new Intent(context, StatisticsActivity.class);
                startActivity(statisticsIntent);
            }
        });
    }
}
