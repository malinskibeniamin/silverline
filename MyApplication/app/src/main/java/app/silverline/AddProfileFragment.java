package app.silverline;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddProfileFragment extends Fragment {
    private ImageView img;
    private Bitmap bitmap;
    private OnFragmentInteractionListener mListener;

    private EditText profileName;
    private Button uploadImageButton;
    public AddProfileFragment() {
    }
    public void setBitmap(Bitmap bitmap){
        this.bitmap = bitmap;
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment AddProfileFragment.
     */
    public static AddProfileFragment newInstance(Bitmap bitmap) {
        AddProfileFragment fragment = new AddProfileFragment();
        fragment.setBitmap(bitmap);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_add_profile, container, false);

        img = fragmentView.findViewById(R.id.test_image);
        profileName = fragmentView.findViewById(R.id.profile_name);
        uploadImageButton = fragmentView.findViewById(R.id.upload_image);
        uploadImageButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                uploadImageButton.setEnabled(false);
                uploadImage();
                uploadImageButton.setEnabled(true);
                profileName.setText("");
                mListener.onFragmentCompleted();
            }
        });
        return fragmentView;
    }

    @Override
    public void onStart(){
        super.onStart();
        if (this.bitmap != null) this.img.setImageBitmap(this.bitmap);
        getView().bringToFront();
    }

    /**
     * upload image to Firebase storage
     */
    public void uploadImage(){
        Log.d("FIREBASE Storage", String.format("Image name: %s", profileName.getText().toString()));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        this.bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        mListener.uploadImage(profileName.getText().toString().trim(), baos.toByteArray());
    }
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (AddProfileFragment.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        void onFragmentCompleted();
        void uploadImage(String name, byte[] bitmap);
    }
}
