package app.silverline;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

public class DeckActivity extends AppCompatActivity
        implements AddProfileFragment.OnFragmentInteractionListener {
    private static final int MAX_IMAGE_BYTES = 100000;
    private StorageReference storageReference;
    private DatabaseReference db;
    private ListView profileList;
    private AddProfileFragment addProfileFragment;
    Deck deck;
    FloatingActionButton addProfileButton;
    private Uri filePath;
    private final int PICK_IMAGE_REQUEST = 71;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck);
        addProfileFragment = (AddProfileFragment) getSupportFragmentManager().findFragmentById(R.id.add_profile_fragment);
        FirebaseStorage storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        db = FirebaseDatabase.getInstance().getReference();
        deck = (Deck) getIntent().getSerializableExtra("deck");
        setTitle(deck.getName());
        System.out.println(deck);
        profileList = findViewById(R.id.profiles_list);
        profileList.setAdapter(new ProfileAdapter(this, deck.getListOfProfiles()));
        addProfileButton = findViewById(R.id.addProfile);
        addProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu options = new PopupMenu(DeckActivity.this, addProfileButton);
                options.getMenuInflater().inflate(R.menu.picture_options, options.getMenu());
                options.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.upload) upload();
                        else if (item.getItemId() == R.id.camera) camera();
                        return true;
                    }
                });

                options.show();
            }
        });
    }

    /**
     * Invokes the upload dialog for choosing files
     */
    public void upload() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                Bitmap resized = Bitmap.createScaledBitmap(bitmap, 640, 640, true);
                if (resized.getByteCount() > MAX_IMAGE_BYTES){
                    int percentage = MAX_IMAGE_BYTES / resized.getByteCount();
                    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                    resized.compress(Bitmap.CompressFormat.PNG, percentage, outStream);
                    resized = bitmap = BitmapFactory.decodeByteArray(outStream.toByteArray(),
                            0, outStream.toByteArray().length);
                }
                addProfileFragment.setBitmap(resized);
                addProfileButton.hide();
                findViewById(R.id.add_profile_container).setVisibility(View.VISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public void camera(){}

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onFragmentCompleted() {
        findViewById(R.id.add_profile_container).setVisibility(View.GONE);
        addProfileButton.show();
    }

    /**
     * The actual method that calls db reference
     * @param name Name of image
     * @param imageBytes The bytes representation of the image
     */
    @Override
    public void uploadImage(String name, byte[] imageBytes) {
        Log.d("FIREBASE Storage", String.format("Image name: %s", name));
        if (imageBytes != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            String resourcePath = String.format("images/%s/", deck.getIdentifier()) + UUID.randomUUID() + ".png";
            StorageReference ref = storageReference.child(resourcePath);

            final StorageMetadata metadata = new StorageMetadata.Builder()
                    .setCustomMetadata("profileName", name)
                    .setCustomMetadata("deckId", deck.getIdentifier())
                    .setCustomMetadata("deckName", deck.getName())
                    .build();
            UploadTask uploadTask = ref.putBytes(imageBytes, metadata);
            uploadTask
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            int n = deck.getListOfProfiles().size();
                            String name = metadata.getCustomMetadata("profileName");
                            String imagePath = taskSnapshot.getMetadata().getPath();
                            Profile newProfile = new ProfileBuilder().setName(name).setImagePath(imagePath).createProfile();
                            db.child(deck.getIdentifier()).child("listOfProfiles").
                                    child(String.valueOf(n)).
                                    setValue(newProfile, new DatabaseReference.CompletionListener() {
                                        @Override
                                        public void onComplete(DatabaseError error, DatabaseReference db) {
                                            if (error != null) {
                                                Log.d("Firebase ERROR", error.getMessage());
                                            } else {
                                                Toast.makeText(DeckActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(DeckActivity.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        }
    }

    class ProfileAdapter extends ArrayAdapter<Profile> {
        private Context context;

        public ProfileAdapter(Context context, ArrayList<Profile> profiles) {
            super(context, 0, profiles);
            this.context = context;
        }

        @Override
        public int getCount() {
            return deck.getListOfProfiles().size();
        }

        @Override
        public Profile getItem(int i) {
            return deck.getListOfProfiles().get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            final Profile profile = getItem(i);
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.profile_button, viewGroup, false);
            }
            TextView profileName = view.findViewById(R.id.profile);
            profileName.setText(profile.getName());
            final ImageView profileImage = view.findViewById(R.id.profile_image);
            profileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    System.out.println(profile);
                    Toast.makeText(DeckActivity.this,
                            String.format("Profile %s clicked.", profile.getName()),
                            Toast.LENGTH_SHORT).show();
                }
            });
            if (profileImage.getDrawable() == null) {
                StorageReference ref = storageReference.child(profile.getImagePath());
                ref.getDownloadUrl()
                        .addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Picasso.with(context).load(uri.toString()).into(profileImage);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                              exception.printStackTrace();
                            }
                        });
            }
            return view;
        }
    }
}
