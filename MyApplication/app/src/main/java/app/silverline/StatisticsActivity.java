package app.silverline;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class StatisticsActivity extends AppCompatActivity {
    private TextView correctAnswers;
    private TextView answers;
    private String numberOfCorrectAnswers;
    private String numberOfAnswers;

    /**
     * Shows the StatisticActivity after game is over to show the statistics of the game.
     */
    private void showStatistics() {
        correctAnswers.setText(numberOfCorrectAnswers);
        answers.setText(numberOfAnswers);
    }

    /**
     * Gets the reference to the elements in the view
     */
    private void initialiseVariables() {
        correctAnswers = findViewById(R.id.numberOfCorrectAnswers);
        answers = findViewById(R.id.numberOfAnswers);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        initialiseVariables();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            numberOfCorrectAnswers = Integer.toString(extras.getInt("correctAnswers"));
            numberOfAnswers = Integer.toString(extras.getInt("answers"));
        }

        showStatistics();
    }
}
