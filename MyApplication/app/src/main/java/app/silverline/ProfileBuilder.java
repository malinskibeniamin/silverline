package app.silverline;

public class ProfileBuilder {
    private String name;
    private String imagePath;

    public ProfileBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ProfileBuilder setImagePath(String imagePath) {
        this.imagePath = imagePath;
        return this;
    }

    public Profile createProfile() {
        return new Profile(name, imagePath);
    }
}